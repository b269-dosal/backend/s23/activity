//console.log("Hellow World");

let trainer = {

    name: "Virgo Dosal Jr.",
    age: 10,
    pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends: {
                hoenn: ["May", "Max"],
                kanto: ["Brock", "Misty"]
            },
    talk: function(){
                console.log('Pikachu I choose you!');
                    }

};

console.log(trainer);
console.log(trainer.name);
console.log("Result of Bracket Notation");
console.log(trainer.pokemon);
trainer.talk();


function Pokemon(name, level) {

    this.name = name;
    this.level = level;
    this.health = 10 * level;
    this.attack = level;


        Pokemon.prototype.tackle = function(target)
    {
        console.log(this.name + " tackled " + target.name);
        target.health = target.health - this.attack;
        console.log(target.name + " health is now reduced to " + target.health);
        console.log('Choose Pokemon: pikachu, charmander, mewtwo');
        console.log('Follow format syntax example: pikachu.tackle(charmander) '); 
        console.log(" ");
        
        if (target.health <= 0) {
            target.faint();
                            }
    };

Pokemon.prototype.faint = function() {
  console.log(this.name + " has fainted.");
};

}

let pikachu = new Pokemon('Pikachu', 30);
let charmander = new Pokemon('Charmander', 29);
let mewtwo = new Pokemon('Mewtwo', 45);

console.log(pikachu);
console.log(charmander);
console.log(mewtwo);

console.log("");
console.log('Choose Pokemon: pikachu, charmander, mewtwo');
console.log('Follow format syntax example: pikachu.tackle(charmander) '); 

/*function choosePokemon() {
  let pokemon = null;
  while (!pokemon) {
    let input = prompt("Choose a Pokemon (Pikachu or Charmander or Mewtwo):");

    if (input === "Pikachu")
    {
      pokemon = pikachu;
    }
     else if (input === "Charmander") 
    {
      pokemon = charmander;
    } 
    else if (input === "Mewtwo") 
    {
      pokemon = mewtwo;
    }
        else 
    
    {
      alert("Invalid Pokemon. Please choose Pikachu or Charmander or Mewtwo.");
    }
  }
  return pokemon;
}


let attacker = choosePokemon();
let target = choosePokemon();


while (target.health > 0) {
  attacker.tackle(target);
  console.log(target.name + "'s health is now " + target.health);
}

console.log(target.name + " has fainted.");*/